var express = require("express");
var path = require("path");
var fs = require("fs");
var bodyParser = require("body-parser");
var MongoClient = require("mongodb").MongoClient;

var connStr = `mongodb://${process.env.MONGO_DB_USERNAME}:${process.env.MONGO_DB_PWD}@${process.env.MONGO_SERVER}`;

app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/profile-picture", (req, res) => {
    var img = fs.readFileSync("image.png");
    res.writeHead(200, { 'Content-Type': 'images/png' });
    res.end(img, 'binary');
});


app.get('/get-profile', (req, res) => {

    MongoClient.connect(connStr, (err, client) => {
        if (err) return res.send({ err: err });

        var db = client.db("user-account");
        var query = { userid: 1 };
        db.collection("users").findOne(query, (err, result) => {
            if (err) return res.send({ err: err });

            client.close();
            res.send(result);
        });
    });
});

app.post("/update-profile", (req, res) => {

    try {
        var userObj = req.body;
        console.log("Connecting to DB...", userObj);

        MongoClient.connect(connStr, (err, client) => {
            if (err) return res.send({ err: err });

            var db = client.db("user-account");
            userObj['userid'] = 1;
            userObj['updated_at'] = new Date().toISOString();
            var query = { userid: 1 };
            var newValues = { $set: userObj };

            console.log("Successfully connected to user-account db.");

            db.collection("users").updateOne(query, newValues, { upsert: true }, (err, response) => {
                if (err) return res.send({ err: err });

                console.log("Successfully updated or inserted. ", response.upsertedId);
                client.close();
                res.send(userObj);
            });
        });
    }
    catch (e) {
        console.log(e);
        res.send({});
    }
});

app.listen(process.env.PORT, () => {
    console.log("Server started.");
});
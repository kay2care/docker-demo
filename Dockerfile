FROM node:14-alpine

RUN mkdir -p /home/paytoken/app
WORKDIR /home/paytoken/app

ENV PORT=4000
EXPOSE 4000

COPY . .
RUN npm install

CMD ["node", "server.js"]
